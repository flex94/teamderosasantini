/**
 * Created by Fabio on 06/03/17.
 */

import React, {Component} from 'react';

import {
    Text
} from 'react-native';

import {Scene, Router} from 'react-native-router-flux';

import Team from './view/team';
import News from './view/news';
import Races from './view/races';

class TabIcon extends React.Component {
    render(){
        return (
            <Text style={{color: this.props.selected ? 'red' :'black'}}>{this.props.title}</Text>
        );
    }
}

export default class TeamDeRosaSantini extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <Router>
                <Scene key="root" tabs={true}>
                    <Scene key="team" title="Team" component={Team} icon={TabIcon}/>
                    <Scene key="news" title="News" component={News} icon={TabIcon}/>
                    <Scene key="races" title="Gare" component={Races} icon={TabIcon}/>
                </Scene>
                <Scene key="player" title="Dettagli" component/>
            </Router>
        );
    }
}